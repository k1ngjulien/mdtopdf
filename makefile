
BUILDCMD=src/mdtopdf init

build:
	echo "Building Container Image"
	$(BUILDCMD)

install: build
	echo "Installing..."
	sudo cp -RT src/ /usr/share/mdtopdf
	sudo ln -sf /usr/share/mdtopdf/mdtopdf /usr/bin/mdtopdf

uninstall:
	echo "Uninstalling..."
	sudo rm /usr/bin/mdtopdf
	sudo rm -r /usr/bin/mdtopdf
