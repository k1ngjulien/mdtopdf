
# mdtopdf - build beautiful documents in without the cruft of latex

Uses `pandoc` inside a docker container to build markdown documents into pdfs using a latex template.

## Setup

### Dependencies


To use this tool, the `docker` command needs to be availabe. If you want to build without `sudo`, you also
need to be able to run the `docker` command without `sudo`.
See [this article](https://docs.docker.com/engine/install/linux-postinstall/) on how to set that up.

Furthermore, `inotify-tools` is used to watch for file changes.

#### Debian/Ubuntu

```
sudo apt-get install inotify-tools
```

#### Arch Based

```
sudo pacman -Sy inotify-tools
```

### Install

Use the makefile to build and install like so:

```
[$]: make install
echo "Building Container Image"
Building Container Image
src/mdtopdf init
~/code/mdtopdf/src ~/code/mdtopdf
building container image 'pandoc-template'...
[+] Building 1.7s (11/11) FINISHED
 => [internal] load .dockerignore                                                                                  0.1s
 => => transferring context: 2B                                                                                    0.0s
 => [internal] load build definition from Dockerfile                                                               0.1s
 => => transferring dockerfile: 250B                                                                               0.0s
 => [internal] load metadata for docker.io/pandoc/extra:2.19                                                       1.4s
 => [1/6] FROM docker.io/pandoc/extra:2.19@sha256:bd6b5eb9d72e91b3a288c09353ad2a693b6519532339ac23eb7b69ce1763b53  0.0s
 => [internal] load build context                                                                                  0.0s
 => => transferring context: 174B                                                                                  0.0s
 => CACHED [2/6] RUN apk add --update alpine-sdk                                                                   0.0s
 => CACHED [3/6] RUN apk add gcc python3-dev linux-headers                                                         0.0s
 => CACHED [4/6] RUN pip install pandoc-fignos pandoc-eqnos pandoc-tablenos pandoc-secnos                          0.0s
 => CACHED [5/6] COPY template /template                                                                           0.0s
 => CACHED [6/6] WORKDIR /data                                                                                     0.0s
 => exporting to image                                                                                             0.0s
 => => exporting layers                                                                                            0.0s
 => => writing image sha256:3ddb4f05eb7229a8da76cf16d51785bd9c1d877f01625d99da4d9974f62a03af                       0.0s
 => => naming to docker.io/library/pandoc-template                                                                 0.0s
done!
~/code/mdtopdf
echo "Installing..."
Installing...
sudo cp -RT src/ /usr/share/mdtopdf
[sudo] password for julian:
sudo ln -sf /usr/share/mdtopdf/mdtopdf /usr/bin/mdtopdf
```

## Usage

See [the example documents](https://gitlab.com/k1ngjulien/mdtopdf/-/tree/main/examples) for details on how to use this software. It's pretty simple to get going.

Feel free to open a merge request if you have more examples to show.

# Author(s)

(c) 2023 Julian Kandlhofer
License: MIT
