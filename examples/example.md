---
title: "Example Document"
author: "Julian Kandlhofer"
date: "2023-06-15"
footer-center: "Hello I am in the Footer :)"
---

# Example Document

This is an example document. Build me by running the following command:

```{#run .shell caption="Build Document"}
mdtopdf run example.md -o example.pdf
```


## Frontmatter

At the top of this document you can see what's called the "Frontmatter".
This contains basic fields about the document such as `title`, `author`, and `date`.

These variables are automatically filled into the template

## CLI args

You can set all the availabe pandoc cli arguments when running `mdtopdf`. They will be forwarded
to pandoc verbatim.


```{#runCliArgs .shell caption="Build with Pandoc CLI arguments"}
mdtopdf run example.md -o example.pdf --shift-heading-level=1
```

See [pandoc manual: options](https://pandoc.org/MANUAL.html#options) for a full list of cli args.

## Defaults

When cli args become to many, you can put them into a `defaults.yml` file and pass that instead

```{#defaults .yml .numberLines caption="Defaults Example"}
template: "template/mytemplate.tex"
variables:
  lof: true # list of figures
  lot: true # list of tables
```

```{#runDefaults .shell caption="Build with Defaults File"}
mdtopdf run example.md -o example.pdf --defaults defaults.yml
```

See [pandoc manual: Defaults files](https://pandoc.org/MANUAL.html#defaults-files) for more information.


## Monitor Mode

To automatically check for changes made, use the `monitor` subcommand.

```{#monitormode .shell caption="Monitor Mode"}
mdtopdf monitor example.md -o example.pdf --defaults defaults.yml
```

This will rebuild the document when any file in the directory of the markdown file or any subdirectory changes.


# Latex Literals

You add literal latex code to your document. Here we are using it to build a list of listings.

```{=latex}
\lstlistoflistings
```
